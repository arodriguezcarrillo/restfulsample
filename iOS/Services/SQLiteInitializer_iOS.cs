﻿using System;
using Xamarin.Forms;
using RESTfulSample.iOS;
using RESTfulSample.Data;
using System.IO;
using SQLite;

[assembly: Dependency(typeof(SQLiteInitializer_iOS))]

namespace RESTfulSample.iOS
{
	public class SQLiteInitializer_iOS : ISQLiteInitializer
	{
		public SQLiteInitializer_iOS ()
		{
		}

		#region ISQLiteInitializer implementation

		public SQLiteConnection GetConnection ()
		{
			var sqliteFilename = "sample_db.db3";
			string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
			var path = Path.Combine(libraryPath, sqliteFilename);
			var conn = new SQLiteConnection (path);
			return conn;
		}

		#endregion
	}

}
