﻿using System;
using RESTfulSample.Droid;
using RESTfulSample.Data;
using SQLite;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteInitializer_Android))]

namespace RESTfulSample.Droid
{
	public class SQLiteInitializer_Android : ISQLiteInitializer
	{
		public SQLiteInitializer_Android ()
		{
		}

		public SQLiteConnection GetConnection () {
			var sqliteFilename = "sample_db.db3";
			string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); // Documents folder
			var path = Path.Combine(documentsPath, sqliteFilename);
			// Create the connection
			var conn = new SQLite.SQLiteConnection(path);
			// Return the database connection
			return conn;
		}
	}
}
