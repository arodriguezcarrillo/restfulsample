﻿using System;
using RESTfulSample.Droid;
using Xamarin.Forms;

[assembly:Dependency(typeof(Localize_Android))]

namespace RESTfulSample.Droid
{
	public class Localize_Android : ILocalize
	{
		public System.Globalization.CultureInfo GetCurrentCultureInfo ()
		{
			var androidLocale = Java.Util.Locale.Default;
			var netLanguage = androidLocale.ToString().Replace ("_", "-"); // turns pt_BR into pt-BR
			return new System.Globalization.CultureInfo(netLanguage);
		}
	}
}
