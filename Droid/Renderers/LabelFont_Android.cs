﻿using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Widget;
using Android.Graphics;
using RESTfulSample;
using RESTfulSample.Droid;

[assembly: ExportRenderer (typeof (LabelFont), typeof (LabelFont_Android))]
namespace RESTfulSample.Droid
{
	public class LabelFont_Android : LabelRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e) {
			base.OnElementChanged (e);
			var label = (TextView)Control; // for example
			Typeface font = Typeface.CreateFromAsset (Forms.Context.Assets, "Raleway.ttf");
			label.Typeface = font;
		}
	}
}

