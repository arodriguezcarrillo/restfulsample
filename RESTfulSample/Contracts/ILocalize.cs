﻿using System;
using System.Globalization;

namespace RESTfulSample
{
	public interface ILocalize
	{
		CultureInfo GetCurrentCultureInfo ();
	}
}

