﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace RESTfulSample
{
	public partial class DecorationCell : ViewCell
	{
		public static readonly BindableProperty TitleProperty = 
			BindableProperty.Create<DecorationCell, string>(ctrl => ctrl.Title,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (DecorationCell)bindable;
					ctrl.Title = newValue;
				},
				defaultBindingMode:BindingMode.TwoWay);

		public string Title {
			get{ return (string)GetValue (TitleProperty); }
			set{
				SetValue (TitleProperty, value);
				this.titleLabel.Text = value;
			}
		}

		public static readonly BindableProperty SubtitleProperty = 
			BindableProperty.Create<DecorationCell, string>(ctrl => ctrl.Subtitle,
				string.Empty,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (DecorationCell)bindable;
					ctrl.Subtitle = newValue;
				},
				defaultBindingMode:BindingMode.TwoWay);

		public string Subtitle {
			get{ return (string)GetValue (SubtitleProperty); }
			set{
				SetValue (SubtitleProperty, value);
				this.subtitleLabel.Text = value;
			}
		}

		public ImageSource Decoration {
			get{
				return this.decorationImage.Source;
			}
			set{
				this.decorationImage.Source = value;
			}
		}

		public DecorationCell ()
		{
			InitializeComponent ();
		}
	}
}

