﻿using System;

using Xamarin.Forms;

namespace RESTfulSample
{
	public class App : Application
	{
		public App ()
		{
			// The root page of your application
			if (Device.OS == TargetPlatform.Android) {
				MainPage = new SplashPage ();
			} else 
				MainPage = new NavigationPage(new RESTfulSample.MainPage());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

