﻿using System;
using System.Collections.Generic;
using RESTfulSample.Data;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace RESTfulSample
{
	public partial class MainPage : ContentPage
	{
		PersonService _service;
		static PersonCollection _data = new PersonCollection();

		public MainPage ()
		{
			InitializeComponent ();

			this.listview.ItemsSource = _data;
			this.listview.ItemTapped += (object sender, ItemTappedEventArgs e) => {
				Person item = (Person)e.Item;
				this.Navigation.PushAsync(
					new DetailPage(item)
				);
			};

			this._service = new PersonService();
			this._service.PersonsUpdated += (object sender, GenericEventArgs<IEnumerable<Person>> e) => {
				_data.Update(e.Data);
			};

			this._service.NetworkUnreachable += async (object sender, EventArgs e) => {
				await DisplayAlert(AppResources.OtherLabel,
					"No se han podido cargar los datos del servicio",
					"OK");
			};

			this.RefreshData ();
		}

		public void listRefresh(object sender, EventArgs e){
			this.RefreshData ();
		}

		public async void RefreshData(){
			try{
				this._service.All ();
			} catch {
				await DisplayAlert ("Atencion",
					"Se ha producido un error cargando los datos", "OK");
			} finally{
				this.listview.IsRefreshing = false;
			}


		}
	}
}

