﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace RESTfulSample
{
	public partial class SplashPage : ContentPage
	{
		public SplashPage ()
		{
			InitializeComponent ();

			Device.StartTimer (TimeSpan.FromSeconds (4),
				() => {
					Navigation.PushModalAsync(new NavigationPage(new RESTfulSample.MainPage()));
					return false;
			});
		}
	}
}

