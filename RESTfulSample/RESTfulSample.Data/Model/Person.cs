﻿using System;
using System.ComponentModel;
using SQLite;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace RESTfulSample.Data
{
	public class Person : ViewModelBase
	{

		[PrimaryKey]
		public int Id {
			get;
			set;
		}

		private string _name;

		[MaxLength(50)]
		public string Name {
			get{
				return this._name;
			}
			set{
				this._name = value;
				this.FirePropertyChangedEvent ("Name");
			}
		}

		private string _surname;
		[MaxLength(50)]
		public string Surname {
			get{
				return this._surname;
			}
			set{
				this._surname = value;
				this.FirePropertyChangedEvent ("Surname");
			}
		}

		[Ignore]
		public IEnumerable<Address> Addresses {
			get;
			set;
		}

		public void UpdateWithData(Person data){
			this.Name = data.Name;
			this.Surname = data.Surname;
			this.Addresses = data.Addresses;
		}
	}
}

