using System;
using System.ComponentModel;
using SQLite;
using System.Collections.ObjectModel;

namespace RESTfulSample.Data
{
	public class Address : ViewModelBase
	{
		[PrimaryKey]
		public int Id {
			get;
			set;
		}

		string _street;
		[MaxLength(200)]
		public string Street {
			get{ return _street; }
			set{ 
				this._street = value;
				this.FirePropertyChangedEvent ("Street");
			}
		}

		int _number;
		public int Number {
			get{ return _number; }
			set{ 
				this._number = value;
				this.FirePropertyChangedEvent ("Number");
			}
		}

		string _zipCode;
		[MaxLength(5)]
		public string ZipCode {
			get{ return _zipCode; }
			set{ 
				this._zipCode = value;
				this.FirePropertyChangedEvent ("ZipCode");
			}
		}

		string _location;
		[MaxLength(50)]
		public string Location {
			get{ return _location; }
			set{ 
				this._location = value;
				this.FirePropertyChangedEvent ("Location");
			}
		}

		public int PersonId {
			get;
			set;
		}

		[Ignore]
		public string CompleteAddress {
			get{
				return String.Format ("{0}, {1}, {2}", Street, Number, ZipCode);
			}
		}
	}

}

