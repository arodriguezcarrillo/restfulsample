﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace RESTfulSample.Data
{
	public class PersonService
	{
		public event EventHandler<GenericEventArgs<IEnumerable<Person>>> PersonsUpdated;
		public event EventHandler NetworkUnreachable;

		PersonDBService _dbService;
		PersonRestService _restService;

		public PersonService ()
		{
			this._dbService = new PersonDBService ();
			this._restService = new PersonRestService ();
		}

		public async void All(){
			var dbData = this._dbService.All ();
			this.FirePersonsUpdated (dbData);

			try{
				var remoteData = await this._restService.All ();
				this._dbService.Update (remoteData);
				this.FirePersonsUpdated (remoteData);
			} catch {
				if (this.NetworkUnreachable != null)
					this.NetworkUnreachable (this, null);
			}
		}
			
		private async void FirePersonsUpdated(IEnumerable<Person> data){
			if (this.PersonsUpdated != null)
				this.PersonsUpdated (this, new GenericEventArgs<IEnumerable<Person>> (data));
		}

	}
}

