﻿using System;
using Xamarin.Forms;
using SQLite;

namespace RESTfulSample.Data
{
	public abstract class DBServiceBase
	{
		protected static SQLiteConnection database;

		public DBServiceBase ()
		{
			if (database == null) {
				database = DependencyService.Get<ISQLiteInitializer> ().GetConnection ();
				database.CreateTable<Person> ();
				database.CreateTable<Address> ();
			}
		}
	}
}

