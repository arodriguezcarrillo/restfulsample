﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace RESTfulSample.Data
{
	public class AddressDBService : DBServiceBase
	{

		private bool Exists(int id){
			return database.Table<Address> ().Any (a => a.Id == id);
		}

		public void Update(Address address){
			if (!Exists (address.Id)) {
				database.Insert (address);
			} else
				database.Update (address);
		}

		public void Update(IEnumerable<Address> addresses){
			foreach (var address in addresses) {
				this.Update (address);
			}
		}

	}
}

