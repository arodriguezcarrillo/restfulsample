﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RESTfulSample.Data
{
	public class PersonDBService : DBServiceBase
	{
		AddressDBService _addressDBService;

		public PersonDBService ()
		{
			this._addressDBService = new AddressDBService ();
		}

		/// <summary>
		/// All this instance.
		/// </summary>
		public IEnumerable<Person> All(){
			return 
				(from person in database.Table<Person>()
				join address in database.Table<Address>() on person.Id equals address.PersonId into addresses
				select new Person{
					Id = person.Id,
					Name = person.Name,
					Surname = person.Surname,
					Addresses = addresses
				}).ToList();
		}

		public bool Update(IEnumerable<Person> data){
			bool result = true;

			foreach (var person in data) {
				if (!this.Update (person))
					result = false;
			}

			return result;
		}

		public bool Update(Person data){
			bool result = false;

			database.BeginTransaction ();

			try{
				if (!Exists (data.Id))
					database.Insert (data);
				else
					database.Update (data);

				if (data.Addresses != null)
					this._addressDBService.Update(data.Addresses);
				
				database.Commit ();
				result = true;
			} catch (Exception excep) {
				database.Rollback ();
				result = false;
			}

			return result;
		}

		private bool Exists(int id){
			return database.Table<Person> ().Any (p => p.Id == id);
		}
	}
}

