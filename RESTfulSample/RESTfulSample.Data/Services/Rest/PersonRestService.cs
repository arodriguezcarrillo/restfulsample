﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace RESTfulSample.Data
{
	public class PersonRestService
	{
		const string _baseURI = "http://vps111149.ovh.net/tickin/";

		public async Task<IEnumerable<Person>> All(){
			var call = new RestCall<IEnumerable<Person>> (
				String.Format ("{0}{1}", _baseURI, "contacts"),
				RestServiceCallType.GET
			);
			return await call.Fetch ();
		}
	}
}

