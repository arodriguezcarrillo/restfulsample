﻿using System;
using SQLite;

namespace RESTfulSample.Data
{
	public interface ISQLiteInitializer {
		SQLiteConnection GetConnection();
	}
}

