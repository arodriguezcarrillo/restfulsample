﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;

namespace RESTfulSample.Data
{
	public class RestCall<T> where T : class
	{
		string _url;
		RestServiceCallType _type;

		public RestCall(
			string url,
			RestServiceCallType type){

			this._url = url;
			this._type = type;

		}

		public async Task<T> Fetch(){
			return await Fetch (new Dictionary<string, object> ());
		}

		public async Task<T> Fetch(Dictionary<string, object> pars){
			string responseString = await MakeRequest (pars);

			return JsonConvert.DeserializeObject<T>(responseString);
		}

		public async Task<string> FetchString(){
			return await FetchString (new Dictionary<string, object> ());
		}

		public async Task<string> FetchString(Dictionary<string, object> pars){
			return await MakeRequest (pars);
		}

		private async Task<string> MakeRequest(Dictionary<string, object> pars){
			HttpWebRequest request;

			if (this._type == RestServiceCallType.GET ||
				this._type == RestServiceCallType.DELETE) {
				string url = this._url;
				if (pars.Count > 0)
					url = String.Format ("{0}?{1}", this._url, GetQuerySerializedParams (pars));
				request = (HttpWebRequest)HttpWebRequest.Create(new Uri (url));
				request.Method = this._type.ToString ();
			} else {
				request = (HttpWebRequest)HttpWebRequest.Create(new Uri (this._url));
				request.Method = this._type.ToString ();
				request.ContentType = "application/json";
				string jsonData = this.GetBodySerializedParams (pars);

				byte[] data = Encoding.UTF8.GetBytes (jsonData);
				Stream stream = await request.GetRequestStreamAsync ();
				stream.Write (data, 0, data.Length);
			}

			try{
				// Send the request to the server and wait for the response:
				using (HttpWebResponse response = (HttpWebResponse) await request.GetResponseAsync ())
				{

					// Get a stream representation of the HTTP web response:
					using (Stream stream = response.GetResponseStream ())
					{
						StreamReader reader = new StreamReader(stream);
						string responseString = reader.ReadToEnd();

						return responseString;
					}
				}
			} catch (WebException excep){
				var stringdata = new StreamReader(excep.Response.GetResponseStream ()).ReadToEnd();
				throw new RestCallException (500, stringdata);
			}
		}

		private string GetQuerySerializedParams(Dictionary<string, object> pars){
			if (pars.Count > 0) {
				List<string> result = new List<string> ();

				foreach (KeyValuePair<string, object> par in pars) {
					result.Add (String.Format ("{0}={1}", par.Key, par.Value));
				}

				return String.Join ("&", result);
			} else
				return string.Empty;
		}

		private string GetBodySerializedParams(Dictionary<string, object> pars){
			return JsonConvert.SerializeObject (pars);
		}
	}

	internal static class WebRequestExtensions
	{
		internal static Task<WebResponse> GetResponseAsync(this WebRequest request)
		{
			TimeSpan timeout = TimeSpan.FromSeconds (60);
			return Task.Factory.StartNew<WebResponse>(() =>
				{
					var t = Task.Factory.FromAsync<WebResponse>(
						request.BeginGetResponse,
						request.EndGetResponse,
						null);

					if (!t.Wait(timeout)) throw new TimeoutException();

					return t.Result;
				});
		}

		internal static Task<Stream> GetRequestStreamAsync(this WebRequest request)
		{
			TimeSpan timeout = TimeSpan.FromSeconds (60);
			return Task.Factory.StartNew<Stream>(() =>
				{
					var t = Task.Factory.FromAsync<Stream>(
						request.BeginGetRequestStream,
						request.EndGetRequestStream,
						null);

					if (!t.Wait(timeout)) throw new TimeoutException();

					return t.Result;
				});
		}
	}

	public class RestCallException : Exception{
		
		public int Status {
			get;
			set;
		}

		public string Message {
			get;
			set;
		}

		public RestCallException (int status, string message)
		{
			this.Status = status;
			this.Message = message;
		}
	}

	public enum RestServiceCallType{
		GET,
		POST,
		PUT,
		DELETE
	}
}

