﻿using System;

namespace RESTfulSample.Data
{
	public class GenericEventArgs<T> : EventArgs where T : class
	{

		public T Data {
			get;
			set;
		}

		public GenericEventArgs (T data)
		{
			this.Data = data;
		}
	}
}

