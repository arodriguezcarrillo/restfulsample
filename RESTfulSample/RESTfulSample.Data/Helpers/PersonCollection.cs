﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

namespace RESTfulSample.Data
{
	public class PersonCollection : ObservableCollection<Person>
	{
		public PersonCollection ()
		{
		}

		public void Update(IEnumerable<Person> input){
			foreach (var item in input) {
				if (!this.Any (i => i.Id == item.Id)) {
					this.Add (item);
				} else {
					this.ReplaceItem (item);
				}
			}

			foreach (var itemToDelete in this.Where(c => !input.Any(i => i.Id == c.Id))) {
				this.Remove (itemToDelete);
			}
		}

		private void ReplaceItem(Person person){
			var i = 0;
			var found = false;

			while (!found && i < this.Count) {
				if (this.ElementAt (i).Id == person.Id) {
					found = true;
					this.ElementAt (i).UpdateWithData (person);
				}

				i++;
			}
		}
	}
}

